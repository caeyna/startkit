<?php

return [
    'verbose' => false,

    'frameworks' => [
        'laravel' => \App\Frameworks\Laravel::class,
        'Lumen' => \App\Frameworks\Lumen::class,
    ],

    'composer' => [
        'require' => [
            'myclabs/php-enum' => '^1.6',
            'spatie/typed' => '^0.0.3',
            'laravel/socialite' => '^3.0',
            'webpatser/laravel-uuid' => '^3.0',
            'spatie/laravel-tags' => '^2.0',
            'laravelcollective/html' => '^5.6',
            'barryvdh/laravel-cors' => '^0.11.0',
            'laravel/passport' => '^6.0',
            'spatie/laravel-fractal' => '^5.3',
            'mccool/laravel-auto-presenter' => '^7.0',
        ],
        'require-dev' => [
            'barryvdh/laravel-debugbar' => '^3.1',
            'barryvdh/laravel-ide-helper' => '^2.4',
        ]
    ],

    'docker-compose' => 'https://gist.githubusercontent.com/michaelbunch/8fbef673f12b4267315be5e5a57762e0/raw/docker-compose.yml',
];
