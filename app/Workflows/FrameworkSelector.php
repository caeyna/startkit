<?php

namespace App\Workflows;

use App\Frameworks\BaseFramework;
use Illuminate\Support\Collection;

class FrameworkSelector extends BaseWorkflow
{
    protected $framework = [];

    public function run()
    {
        $availableFrameworks = $this->getAvailableFrameworks();
        $selectedFramework = $this->showFrameworkMenu($availableFrameworks);

        $framework = $availableFrameworks[$selectedFramework];
        $framework->setVersion($this->showVersionsMenu($framework));

        return $framework;
    }

    public function showFrameworkMenu(Collection $frameworks)
    {
        return cli()
            ->br()
            ->radio(
                'Select your base framework:',
                $frameworks->map(function ($framework) {
                    return $framework->getName();
                })->toArray()
            )
            ->prompt();
    }

    public function showVersionsMenu(BaseFramework $framework)
    {
        return cli()
            ->br()
            ->radio(
                'Select the version of "'.$framework->getName().'" you would like installed:',
                $framework->fetchVersions()['latest_minors']
            )
            ->prompt();
    }

    public function getAvailableFrameworks() : Collection
    {
        $frameworks = config('startkit.frameworks');
        return collect($frameworks)
            ->map(function ($framework) {
                return app()->make($framework);
            });
    }

    public function getFramework(string $framework) : BaseFramework
    {
        $class = config('startkit.frameworks.'.$framework);
        return app()->make($class);
    }
}
