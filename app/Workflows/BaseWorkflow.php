<?php

namespace App\Workflows;

use Illuminate\Console\Command;

abstract class BaseWorkflow
{
    protected $command;

    public function __construct(Command $command)
    {
        $this->command = $command;
    }

    abstract public function run();
}
