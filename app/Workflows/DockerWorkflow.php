<?php

namespace App\Workflows;

use App\Handlers\DockerCompose;
use Illuminate\Support\Facades\File;

class DockerWorkflow extends BaseWorkflow
{
    protected $installable = false;

    public function run()
    {
        $answer = cli()->br()->confirm('Would you like to add Docker support for local development?');
        if ($answer->confirmed()) {
            $this->installable = true;
        }
        return $this;
    }

    public function setupFromGist(string $folder)
    {
        File::copy(
            config('startkit.docker-compose'),
            base_path() . '/' . $folder . '/docker-compose.yml'
        );

        // $gist = file_get_contents(config('bootstrapper.docker-gist'));
        // file_put_contents($this->command->argument('name').'/docker-compose.yml', $gist);
    }

    public function install(string $projectName)
    {
        if ($this->isInstallable()) {
            cli()->lightBlue()->inline(' - Configuring local Docker environment... ');
            $this->setupFromGist($projectName);
            cli()->green()->out('done');
            cli()->lightBlue()->inline(' - Starting Docker containers... ');
            DockerCompose::up($projectName);
            cli()->green()->out('done');
        }
    }

    public function isInstallable() : bool
    {
        return $this->installable;
    }
}
