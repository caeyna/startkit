<?php

namespace App\Workflows;

use App\Handlers\Composer;
use Illuminate\Support\Facades\File;

class ComposerWorkflow extends BaseWorkflow
{
    protected $composerJson;

    protected $selectedPackages = [
        'require' => [],
        'require-dev' => [],
    ];

    public function run(bool $dev = false)
    {
        $require = 'require';
        if ($dev) {
            $require .= '-dev';
        }

        $this->selectedPackages[$require] = $this->showPackagesMenu(config('startkit.composer.' . $require), $dev);
        return $this;
    }

    public function showPackagesMenu(array $packages, bool $dev = false)
    {
        return cli()
            ->br()
            ->checkboxes(
                'Select the Composer' . ($dev ? ' development' : '') . ' packages you would like to install:',
                array_keys($packages)
            )
            ->prompt();
    }

    public function install(string $projectName)
    {
        $this->parseComposerJson($projectName);
        cli()->lightBlue()->inline(' - Configuring Composer require packages... ');
        $this->addRequire();
        cli()->green()->out('done');
        cli()->lightBlue()->inline(' - Configuring Composer require-dev packages... ');
        $this->addRequireDev();
        cli()->green()->out('done');
        cli()->lightBlue()->inline(' - Updated Composer... ');
        $this->writeComposerJson($projectName);
        Composer::update($projectName);
        cli()->green()->out('done');
    }

    public function parseComposerJson(string $projectName)
    {
        $this->composerJson = json_decode(File::get(base_path() . '/' . $projectName . '/composer.json', true));
    }

    public function addRequire()
    {
        $require = 'require';
        foreach ($this->selectedPackages['require'] as $package) {
            $this->composerJson->$require->$package = config('startkit.composer.require.' . $package);
        }
    }

    public function addRequireDev()
    {
        $require = 'require-dev';
        foreach ($this->selectedPackages['require-dev'] as $package) {
            $this->composerJson->$require->$package = config('startkit.composer.require-dev.' . $package);
        }
    }

    public function writeComposerJson(string $projectName)
    {
        File::put(
            base_path() . '/' . $projectName . '/composer.json',
            json_encode($this->composerJson, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)
        );
    }

    public function getPackages()
    {
        return $this->selectedPackages;
    }
}
