<?php

namespace App\Frameworks;

use Composer\Semver\Comparator;
use Illuminate\Support\Collection;

class Laravel extends BaseFramework
{
    protected $name = 'Laravel Framework';
    protected $description = 'The official Laravel framework package.';
    protected $package = 'laravel/laravel';
    protected $minVersion = 'v5.5.0'; // Package discovery versions only

    public function parseVersions(Collection $versions): Collection
    {
        return $versions
            ->filter(function ($item) {
                return substr($item, 0, 1) === 'v';
            })
            ->sort()
            ->reverse()
            ->pipe(function ($collection) {
                $hold = ['major' => [], 'minor' => []];
                $versions = [
                    'latest' => $collection->first(),
                    'latest_majors' => [],
                    'latest_minors' => [],
                ];
                foreach ($collection as $version) {
                    if (Comparator::lessThan($version, $this->minVersion)) {
                        continue;
                    }
                    $sections = explode('.', $version);
                    if (!in_array($sections[0], $hold['major'])) {
                        $hold['major'][] = $sections[0];
                        $versions['latest_majors'][$version] = $version;
                    }

                    if (!in_array($sections[0] . '.' . $sections[1], $hold['minor'])) {
                        $hold['minor'][] = $sections[0] . '.' . $sections[1];
                        $versions['latest_minors'][$version] = $version;
                    }
                }
                return collect($versions);
            });
    }
}
