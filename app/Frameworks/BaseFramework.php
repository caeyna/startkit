<?php

namespace App\Frameworks;

use App\Handlers\ProcessHandler;
use Illuminate\Support\Collection;
use App\Handlers\Composer;

abstract class BaseFramework
{
    protected $name;
    protected $description;
    protected $package;
    protected $version = 'latest';

    public function getName() : string
    {
        return $this->name;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function getVersion() : string
    {
        return $this->version;
    }

    public function setVersion(string $version) : void
    {
        $this->version = $version;
    }

    public function fetchVersions() : Collection
    {
        $versions = explode(
            ':',
            ProcessHandler::run('composer show ' . $this->package . ' --all | grep versions')
        );
        return $this->parseVersions(
            collect(explode(',', $versions[1]))
                ->map(function ($item) {
                    return trim($item);
                })
        );
    }

    abstract public function parseVersions(Collection $versions) : Collection;

    public function install(string $folder)
    {
        cli()->lightBlue()->inline(' - Installing framework... ');
        Composer::createProject(
            $this->package,
            $folder,
            $this->version
        );
        cli()->green()->out('done');
    }
}
