<?php

namespace App\Handlers;

class DockerCompose
{
    public static function up(string $folder = '', bool $daemonized = true)
    {
        $command = 'docker-compose up';
        if (!empty(trim($folder))) {
            $command = 'cd ' . $folder . ' && ' . $command;
        }

        if ($daemonized) {
            $command .= ' -d';
        }
        ProcessHandler::run($command);
    }
}
