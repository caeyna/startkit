<?php

namespace App\Handlers;

class Composer
{
    const COMPOSER = 'composer';

    public static function createProject(string $package, string $folder, string $version = 'latest')
    {
        $command = self::COMPOSER.' create-project --prefer-dist ';
        $command .= $package;
        $command .= ' '.$folder;
        if ($version !== 'latest') {
            $command .= ' '.$version;
        }
        ProcessHandler::run($command);
    }

    public static function require(string $package, bool $isDev = false)
    {
        $command = self::COMPOSER.' require '.$package;
        if ($isDev) {
            $command .= ' --dev';
        }
        ProcessHandler::run($command);
    }

    public static function update(string $workingDir = '.')
    {
        $command = self::COMPOSER . ' update --working-dir=' . $workingDir;
        ProcessHandler::run($command);
    }
}
