<?php

namespace App\Handlers;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class ProcessHandler
{
    public static function run(string $command) : string
    {
        $process = new Process($command);
        $process->setTimeout(500);
        $process->run(function ($type, $buffer) {
            if (config('startkit.verbose')) {
                echo $buffer;
            }
        });

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $process->getOutput();
    }
}
