<?php

namespace App\Commands;

use App\Workflows\ComposerWorkflow;
use App\Workflows\DockerWorkflow;
use App\Workflows\FrameworkSelector;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\File;
use LaravelZero\Framework\Commands\Command;
use Symfony\Component\Console\Output\OutputInterface;

class NewCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'new {name=NewProject}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Create a new project';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $projectName = $this->argument('name');
        config([
            'startkit.verbose' => $this->getOutput()->getVerbosity() === OutputInterface::VERBOSITY_VERBOSE
                ? true
                : false
        ]);

        $this->validateProjectFolderExits($projectName);

        cli()->green()->invert()->out('   Welcome to StartKit! Let\'s get your new project bootstrapped!   ');
        $framework = (new FrameworkSelector($this))->run();
        $composer = (new ComposerWorkflow($this))->run()->run(true);
        $docker = (new DockerWorkflow($this))->run();

        cli()->br()->green()->invert()->out('   Here\'s what will be installed:   ');
        $summary = cli()->padding(20);
        $summary->label('Framework')->result($framework->getName());
        $summary->label('Version')->result($framework->getVersion());
        $summary->label('Docker')->result(($docker->isInstallable() ? 'Yes' : 'No'));
        cli()->br();
        cli()->out('Composer Packages');
        $packages = $composer->getPackages();
        $summary->label('Require')->result(implode(', ', $packages['require']));
        $summary->label('Require-dev')->result(implode(', ', $packages['require-dev']));

        $input = cli()->br()->confirm('Continue?');
        if ($input->confirmed()) {
            cli()->br()->out('Creating project...');
            $framework->install($projectName);
            $composer->install($projectName);
            $docker->install($projectName);
            cli()->br()->green()->out('Bootstrapping complete! Happy coding!');
        }
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }

    public function validateProjectFolderExits(string $folder) : void
    {
        if (File::exists($folder)) {
            $this->error('The provided project name already exists in the path. Please try another name.');
            exit();
        }
    }
}
