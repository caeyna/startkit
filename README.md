# StartKit

A Laravel/Lumen project bootstrapping command line tool.

## Overview

This tool simplifies the repetative task of setting up a new Laravel/Lumen project. After installing,
run the following:

```bash
php startkit new <project-name>
```

You will be prompted to select the following:

* A base framework and version (Only version 5.5 and up)
* Composer packages from a precompiled list.
* Support for local Docker development.

When the installation is complete you will have a fully installed and running development environment. Go
to `http://localhost:8000` to view the application.